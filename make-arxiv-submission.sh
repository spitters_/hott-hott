#!/bin/bash

# Create a .zip file containing all files needed for arXiv submission,
# for easy uploading

master_name=hott-arxiv

latexmk -pdf ${master_name}

current_date=$(date "+%Y-%m-%d")
submission_name="${current_date}-arxiv-submission"
submission_dir_name="${submission_name}-temp"

# create temporary directory of files by arXiv:
rm -rf $submission_dir_name
mkdir $submission_dir_name
cp *.tex ${submission_dir_name}/
cp ${master_name}.bbl ${submission_dir_name}/

# zip, from in dir so files are at root level of archive
cd $submission_dir_name
rm hott-cpp.tex sigplan*.tex
zip -r ../${submission_name}.zip *
cd ..

# clean up temp directory
rm -rf $submission_dir_name
