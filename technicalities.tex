
%\section{Coq technicalities}\label{sec:coq-technicalities}
\subsection{Transparency and Opacity}\label{sec:opaque}
Since equality in homotopy type theory is proof-relevant, our lemmas are more often transparent than in most Coq
developments. This means that much more care must be taken to construct the
right, coherent, proof terms. An example is the proof of
the Eckmann-Hilton theorem for identity types (Theorem 2.1.6 of the HoTT book), which is given explicitly using (higher) composites and whiskerings:
\begin{lstlisting}
Definition eckmann_hilton
  {A : Type} {x:A} (p q : 1 = 1 :> (x = x)) :
  p @ q = q @ p
  :=  (whiskerR_p1 p @@ whiskerL_1p q)^
      @ (concat_p1 _ @@ concat_p1 _)
      @ (concat_1p _ @@ concat_1p _)
      @ (concat_whisker _ _ _ _ p q)
      @ (concat_1p _ @@ concat_1p _)^
      @ (concat_p1 _ @@ concat_p1 _)^
      @ (whiskerL_1p q @@ whiskerR_p1 p).
\end{lstlisting}
Here \lstinline|@| denotes concatenation of paths and \lstinline|@@|
denotes horizontal composition of 2-dimensional paths.
%
It might be easier to find \emph{a} proof using rewrite-type tactics, but we want this particular one.

We could benefit from good support for construction of explicit proof terms.
Sozeau's dependent pattern matching compiler~\citep{sozeau2010equations} aims to
provide such support in Coq. It is similar to how Agda works. Unfortunately, it currently depends on both the
equality type being in $\prop$ and uniqueness of identity proofs,
which is incompatible with univalence. This issue has been solved in
Agda~\citep{cockx2014pattern} and is currently being adapted to Coq as
part of the Equations package~\citep{mangin2015equations}.
The prelude of the HoTT library can already be processed using Equations, and we hope
to be able to use it for the entire library in the future.

\subsection{Automation}\label{sec:automation}

Once we collected a large number of lemmas about paths, we organized
them in a rewrite database and used it to simplify proofs.
Currently, we rewrite terms in a standard fashion, that is with
$J$-elimination. However, we are likely to obtain better proof terms
by using the technology of generalized rewriting~\citep{sozeau2010new}, which would also
know how to rewrite with equivalences, and other suitable general relations.
For instance,
\begin{lstlisting}
forall {A B:Type},
  IsHProp A -> (A <~>B) -> IsHProp B
\end{lstlisting}
can be proved by rewriting without invoking
the univalence axiom.
This is done by first showing that the basic type constructors $\Pi,\Sigma$
respect equivalence, and that contractibility transfers along
equivalence. All concepts built from these will then also respect
equivalence. So, surprisingly, even now that we have quotients in our
type theory, the technology initially developed for setoid rewriting
is still useful.

% We really should get setoid-rewriting to work:
%https://www.irif.univ-paris-diderot.fr/~sozeau/research/publications/Coq_support_for_HoTT-HoTTUF-290615.pdf
% (Setoid) Rewriting, (Also discussed Equations)
% This should also make hott_simpl and path_hints more useful.
% MS:
% https://github.com/mattam82/Coq-Equations/blob/8.5/examples/HoTT_light.v shows an example use,
% in isequiv_inverse of generalize rewriting with type-valued equality.
% https://github.com/HoTT/HoTT/blob/master/STYLE.md#available-tactics
% They are described more fully, usually with examples, in the files where they are defined.
Two other tactics are worth mentioning.
First, we have a tactic \lstinline|transparent assert|, like ordinary \lstinline|assert| (allowing on-the-fly interactive proof of assertions) except that the term produced remains transparent in the rest of the proof. (Meanwhile, this has been ported for inclusion in standard Coq.)
% https://github.com/coq/coq/pull/201/files/206103728f525bbff8f0cc46374354faad12d0a1..111ef28ef649bc461a217741845ebc2a94764b51
Second, we implemented a custom version of the \lstinline|apply| tactic which uses a more powerful unification algorithm than the one used by the standard \lstinline|apply| tactic. This idea is inspired by similar techniques in ssreflect~\citep{gonthier:inria-00258384}.
% Not sure what to say here. The tactic is only used a few times.
% \item path induction hammer\ldots %Tactics.v
% Overture.v


% We have reimplemented a number of tactics in the
% Ltac language and developed parts of the library from scratch.
% Some changes are here. Not sure what to say about them.
% https://github.com/HoTT/HoTT/tree/master/coq/theories/Init

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:
