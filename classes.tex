\subsection{Type Classes}\label{sec:type-classes}
% Overture.v

Coq's type class system is a very convenient mechanism for automatic derivation of
instances of structures. The instance search follows a logic programming discipline which
is sometimes difficult to predict and control, a potential problem for proof-relevant
settings such as homotopy type theory. Nevertheless, it is safe to use the type class
mechanism as long as it is employed to find only mere propositions, as all their instances are
equal. Luckily, being an equivalence, deriving a truncation level, and placing a type into
a subuniverse, are all of this kind, and so our library does use type classes in these
non-problematic cases. We follow the development style of the math-classes
library~\citep{math-classes,KrebbersSpitters,casteran:hal-00702455}, but since we have
a better behaved equality and quotients, we can avoid an extensive use of setoids.
% The algebraic hierarchy that Gilbert used for the real numbers
% follows this approach.

As is well known, one can only push the type class mechanism so far. For
instance, we cannot add a rule for automatic instantiation of inverses of equivalence, as
it makes Coq look for ``the inverse of the inverse of the inverse \dots''.

\paragraph{Transfer from $\Sigma$-types to record types}

Type classes are (dependently typed) record types, so many of the central concepts in the
library are expressed as record types (rather than nested $\Sigma$-types). To avoid proving a
series of general lemmas over and over, separately for each record type,
we implemented a tactic \lstinline|issig| that allows us to automatically transfer general facts about
iterated $\Sigma$-types to record types. For example, \lstinline|Contr| and
\lstinline|IsEquiv| are record types, and the tactic is able to automatically prove that
% Record.v
\begin{lstlisting}
  forall A,  {x : A & forall y:A, x = y} <~> Contr A.
\end{lstlisting}
and
\begin{lstlisting}
  forall A B, forall f : A -> B,
   IsEquiv f <~>
     {g : B -> A &
       {r : Sect g f &
       {s : Sect f g &
         forall x : A, r (f x) = ap f (s x) }}}.
\end{lstlisting}
%
% https://github.com/HoTT/HoTT/blob/master/theories/Factorization.v
A fairly substantial use of this tactic arises in the development of
factorization systems (e.g.\ epi-mono factorization), for the proof of uniqueness of factorizations (our \lstinline|path_factorization|, Theorem
7.6.6 in the HoTT book).   The book proof expresses the uniqueness using a threefold nesting of
$\Sigma$-types followed by a threefold cartesian product. In the formalization this
corresponds to a record type with six fields. The formal proof requires delicate
transformations between records and $\Sigma$-types that would be very laborious to perform
without \lstinline|issig|.

\paragraph{The $\eta$-rule for record types}

Recent versions of Coq support an extensionality $\eta$-rule for record types,
implemented by primitive projections.
%
When we adapted the library to use primitive projections, the compilation time for the
entire library dropped by a factor of two. We were also able to replace eight different
kinds of natural transformations with a single one, remove applications of function
extensionality from a number of proofs, and greatly speed up the tactics for transfer
between $\Sigma$-types are record types.

\paragraph{Type classes for axioms}

We also use type classes to track the use of the univalence and
function extensionality axioms.  These axioms are defined to depend on
an instance of a class that inhabits a dummy type.  For instance, here
is function extensionality:
\begin{lstlisting}
Monomorphic Axiom dummy_fe : Type0.
Monomorphic Class Funext := { fev : dummy_fe }.
Axiom isequiv_apD10 :
  forall `{Funext} (A : Type) (P : A -> Type) f g,
    IsEquiv (@apD10 A P f g).
\end{lstlisting}
Here, \lstinline|apD10| is the canonical map from paths in the function type to homotopies between functions.

Any theorem that uses function extensionality must then state this
fact explicitly with a \lstinline|`{Funext}| assumption.  This makes
it easy to tell which parts of the library depend on which axioms,
without the need for \lstinline|Print Assumptions|.  We make
\lstinline|Funext| inhabit a dummy type rather than the actual type of
\lstinline|isequiv_apD10| so that the latter can be used at multiple
universe levels with only a single assumption of \lstinline|Funext|.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:

