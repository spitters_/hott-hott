\section{\cpporarxiv{Higher Inductive Types}{Higher inductive types}}\label{HIT}

Higher inductive types are one of the main novelties in homotopy type
theory. Where usual inductive types allow us to freely generate terms
in a type, \emph{higher} inductive types also allow us to
\emph{freely} generate equalities. Examples include the interval (two
points and a path), the circle (a point and a self loop), suspensions,
set quotients, and more complex examples that we shall discuss briefly.

Coq does not implement higher inductive types natively, so we simulate
them using Licata's trick~\citep{licata2013calculating}.  This method
was originally used in Agda; to make it possible in Coq, experimental
\emph{private inductive types} had to be added~\citep{Bertot}.  Private
inductive types are defined inside a module, within which they behave
as usual.  Outside the module their induction principles and pattern
matching are no longer available; but functions that were defined
inside the module using those principles can still be called, and
importantly still compute on constructors.

Licata's trick uses this to implement higher inductive types whose
induction principles compute definitionally on point-constructors.
For instance, the definition of the interval is shown in
Figure~\ref{fig:interval}.  Here \lstinline|#| denotes transport, \lstinline|@| is concatenation of paths, and \lstinline|apD| is application of a dependent function
to a path.  Importantly, \lstinline|interval_rec a b p zero|
\emph{reduces} to \lstinline|a|, because \lstinline|zero| is actually
a constructor of \lstinline|interval|.
%
The axiom \lstinline|seg| could be used to derive a
contradiction within the module; but this is not possible
outside the module, so all we need to do is check that the code inside
the module is safe.

\begin{figure}[t]
\centering
\begin{lstlisting}
Module Export Interval.

Private Inductive interval : Type1 :=
  | zero : interval
  | one : interval.

Axiom seg : zero = one.

Definition interval_ind (P : interval -> Type)
  (a : P zero) (b : P one) (p : seg # a = b)
  : forall x:interval, P x
  := fun x => (match x return _ -> P x with
                | zero => fun _ => a
                | one  => fun _ => b
              end) p.

Axiom interval_ind_beta_seg :
  forall (P : interval -> Type)
    (a : P zero) (b : P one) (p : seg # a = b),
    apD (interval_ind P a b p) seg = p.

End Interval.

Definition interval_rec (P : Type) (a b : P) (p : a = b)
  : interval -> P :=
  interval_ind (fun _ => P) a b (transport_const _ _ @ p).

Definition interval_rec_beta_seg
  (P : Type) (a b : P) (p : a = b) :
  ap (interval_rec P a b p) seg = p.
\end{lstlisting}
  \caption{The higher inductive interval}
  \label{fig:interval}
\end{figure}

Note that in \lstinline|interval_ind| rather than simply matching
on $x$, we apply the match statement to the path hypothesis $p$ (but
then never use it).  If the path-hypotheses were not
``used'' anywhere in the match, Coq would notice this and
conclude that two invocations of the induction principle ought to be
judgmentally equal as soon as they have the same point-hypotheses,
even if their path-hypotheses differ, also leading to inconsistency.
(This was noticed by Bordg.)

The library currently includes higher inductive definitions of the
interval, the circle, suspensions, truncations, set-quotients, and
homotopy colimits such as coequalizers and pushouts, as well as the
flattening lemma, all from Chapter~6 of the HoTT book.  The library
also formalizes some basic synthetic homotopy theory from Chapter~8,
such as using the encode-decode method to prove that the fundamental
group of the circle is the integers.

The library contains some of the more experimental
higher inductive types from the HoTT book.  The cumulative hierarchy
of well-founded sets from Chapter~10 was formalized by
\citet{Ledent} and is now part of the library.  Two higher
inductive-inductive types from Chapter~11, the Cauchy reals
and the surreal numbers
are trickier. Even ordinary inductive-inductive
types~\citep{nordvallforsbergSetzer2012finIndind} are not supported in
Coq 8.5, but fortunately it is possible to simulate them using private
inductive types.  The surreals are thus in the library, while the Cauchy
reals have been formalized elsewhere~\citep{Gilbert}, as has another example of a higher inductive-inductive type, the
partiality monad~\citep{Altenkirch}. The formalization of the Cauchy reals and the partiality monad is based on an
experimental Coq implementation of inductive-inductive
types.\footnote{\cpporarxiv{Sozeau}{Sozeau}, \url{https://github.com/mattam82/coq/tree/IR}} We plan to include these developments into the library as soon as the support for inductive-inductive types is sufficiently mature.

Finally, we note that since private inductive types do not technically
use the path constructors, higher inductive types defined in this way
sometimes end up in a universe that is too low.  This can usually be
fixed with explicit universe annotations.

%  that assign the maximum of the
% universes of point \emph{and} path constructors. A special Coq extension was needed to accomplish
% this.

% Andrej: I removed the quotient example because it's a lie. We do not actually
% do it in the code, see https://github.com/HoTT/HoTT/blob/master/theories/HIT/quotient.v#L33
%
% \begin{lstlisting}
% Notation is_mere_relation R := (forall x y, IsHProp (R x y)). 
% Private Inductive quotient (sR : is_mere_relation R@{U V}):
%   Type@{max U V} := | class_of : (A:Type@{U}) -> quotient sR.
% \end{lstlisting}
%
% to the higher inductive type.
% https://github.com/HoTT/HoTT/issues/565
% https://github.com/HoTT/HoTT/blob/master/STYLE.md#universes-and-hits


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:
