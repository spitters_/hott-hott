\section{\cpporarxiv{Related Work}{Related work}}\label{sec:related-work}

The HoTT library was initiated as an attempt to understand the contents of Voevodsky's
Foundations library~\citep{VV-foundations}. In the beginning we closely followed the order
of development of concepts in the Foundations library, but usually with our own proofs.

Nowadays the Foundations library has been incorporated 
into UniMath \citep{UniMath}. UniMath and HoTT still share many ideas about
how to formalize homotopy type theory, with a few differences: UniMath uses
the inconsistent assumption $\type\,{:}\,\type$ as a simplifying device, whereas we deal
with all the complexities of universe polymorphism; UniMath takes a conservative approach
with respect to advanced Coq technology, whereas our library actually inspired a
number of Coq features and serves as a testbed for new ones; finally, the HoTT library
uses higher inductive types, which are generally avoided by UniMath.

Various external developments have also used the HoTT library as a
base, including libraries for the interpretation of the database query language SQL
in homotopy theory~\citep{HoTTSQL}, and for monadic semantics of probabilistic computation with continuous
data types~\citep{FS}.

There are implementations of homotopy type theory in other proof assistants. The HoTT-Agda
library\footnote{\url{https://github.com/HoTT/HoTT-Agda}} was initially started as a
parallel development in Agda, but quickly took a different direction and experienced
extensive development of synthetic homotopy theory. The private inductive types were
inspired by Licata's trick, which was originally implemented in Agda.

A new formalization of homotopy type theory in the proof assistant Lean~\citep{lean} is
growing at an impressive rate. Lean’s type class system is a particularly useful feature. 

A related development is the prototype implementation of cubical type
theory~\citep{Cubical}, which includes a computational interpretation of univalence and (some)
higher inductive types. This improves on one of the limitations of our setting, the use of axioms for
univalence and function extensionality, which block computation in some proofs. 
We look forward to the integration of this technology in proof assistants.

\section{Conclusion}\label{sec:conclusion}

We have developed a large, well-designed, and well-docu\-mented library for homotopy type
theory, which formalizes a large portion of the HoTT book, including higher inductive
types, and employs universe polymorphism.
The library has successfully been used as a basis for several more specific
formalizations.  It serves as a testing ground for new Coq features,
as well as a foundation on which to experiment with formalizing new
ideas and applications of homotopy type theory.

% A major concern for any piece of formalized mathematics is trust. How do we know that the
% HoTT library, which uses much more than just Martin-L\"of type theory, is safe? It may
% well be that the univalence axiom, pattern matching, universe polymorphism, type classes,
% private inductive types, etc., together form an inconsistent system. This is a major
% concern not just for us, but for every user of complex proof assistants, and it has been
% addressed many times in the past.

% There are several possible ways to tackle safety. For instance, the UniMath team avoids
% advanced Coq technology and strives to keep their development as close as possible to
% standard Martin-L\"of type theory, albeit they assume $\type\,{:}\,\type$ from the onset.

% Another possibility is to give a semantic account of the extensions of type theory that we
% use in the formalization. While any particular fragment has been studied here or there,
% combining them all to give a unified semantic account of homotopy type theory together
% with Coq's extensions seems a daunting task.

% We feel that proof assistants and computerized formalization of mathematics is at such an
% early stage that it is well worth experimenting in order to learn what works and what does
% not, even at the risk of introducing an inconsistency. In any case, the doubtful reader
% should keep in mind that the standard of proof in formalized mathematics is a great deal
% higher than the generally accepted level of rigor among mathematicians.

%%%%%%%%%%

% \paragraph{Future Work: Semantics}\label{sec:semantics}
% One may wonder whether our changes and extensions to the Coq type
% theory are still consistent.  Semantics for homotopy type theory is a
% topic of active research, but much is known and more is conjectured.
% Our goal has not been to stick to a theory with rock-solid semantics,
% but instead to focus on experimental development within the existing
% Coq-system, thereby also suggesting directions for future work on
% semantics.

% % Unimath aims to be very safe, it would be good to anticipate criticism.
% The basic $\Pi\Sigma$-type theory with cumulative univalent universes can be
% modeled in simplicial
% sets~\citep{VoevodskySimp,kapulkin2012simplicial}.
% There is no impredicative $prop$ in this model, but we do not use
% Coq's impredicative $prop$ (note that polymorphic universes can only
% be instantiated by universes greater or equal to $\set$, hence no
% instantiation of a universe with $\prop$~is possible).

% The simplicial set model can be extended to $W$-types~\citep{WinHoTT}; general
% and higher inductive types are still open,
% but some positive results are known~\citep{ls:hits}.
% % $M$-types have been developed in a type
% % theory with $W$-types~\citep{ahrens2015non}. However, it is not clear to
% % what extent this covers all of Coq's coinductive types. In any case,
% % coinductive types are not used in our development.
% However, few guarantees are known about
% the experimental implementation of private inductive types.
% Record types are just $\Sigma$-types, and type classes are just
% records with instance search outside the kernel, so these are
% unproblematic.
% And universe polymorphism can, for the most part, be given semantics
% by reduction to the non-polymorphic Calculus of Inductive
% Constructions, so that it has the same meta-theoretic
% properties~\citep{sozeau2014universe}.
% The exception to this is universe-polymorphic modules, which to our
% knowledge have not been studied semantically; semantic work on modules
% seems limited to the non-dependently-typed case~\citep{Moggi:modules}.



%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:
