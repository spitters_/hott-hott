\section{\cpporarxiv{Modalities and Modules}{Modalities and modules}}\label{sec:modules}
% https://homotopytypetheory.org/2015/07/05/modules-for-modalities/

Type-theoretic modalities, cf.\ Section~7.7 of the HoTT book and~\citep{RSS}, generalize the $n$-truncations in homotopy type theory. They were used in \emph{cohesive} homotopy type theory of \citet{schreibershulman2012}.
While we were able to formalize proofs and constructions about modalities in a straightforward manner, the overall
architecture of the definitions was quite tricky to implement.

Chapter~7 of the HoTT book contains a number of facts about truncations and
connectedness which actually hold (as
remarked therein) for any modality, with exactly the same proofs.
In the library we therefore formalized general facts about modalities,
following Chapter~7, and then instantiated them to truncations, which
we defined as particular instances of modalities.

A modality is an operator $\bigcirc$ which acts on types and satisfies a universal
property that quantifies over all types. One might expect the formalization of~$\bigcirc$ to be a
universe-polymorphic record type whose fields are the operator and its universal property.
This does not work however, because the fields would share a common universally quantified
universe index~$i$, and would thus express the wrong universal property. That is, we need
to express that $\bigcirc$ at level~$i$ has the universal propety with respect to every
level~$j$, not only~$i$.
%
We needed a construct like record types, but allowing each field to be
individually universe-polymorphic.

The solution was an undocumented feature of the Coq module system, which we briefly
review. Coq provides ML-style modules similar to those of OCaml, the programming language
used to implement Coq. Modules and type classes are similar, but the former are not
first-class objects and therefore cannot be used for formalization of structures. Instead,
modules serve a software-engineering purpose: they are used to organize source code into
units, and to control visibility of implementation details. By a fortunate design choice,
each entry in a module carries its own universal quantification over the universe levels.

The library formalizes a modality as a module type so that it can have the correct
universal property. However, we lose many of the benefits of record types and type
classes, as well as notational conveniences.

First, to pass a simple argument to a parametrized module (a functor in the
ML-terminology), we need to wrap it into a module, which considerably increases the
complexity of the code. Even worse, it prevents us from defining families of modalities,
such as $n$-truncations indexed by a truncation index~$n$. We used the standard trick of
passing from families to display maps, and included in the definition of modality an
additional field \lstinline|Modality : Type|, to be used as the domain of a display map
which encodes a family of modalities. For instance, truncations are implemented as a
single module which sets \lstinline|Modality| to \lstinline|trunc_index|.

Second, Coq is very strict about matching modules against module
types: it insists that the universe parameters and constraints match
the specified ones exactly. Thus, in the formalization we had to
battle Coq's universe heuristics and tightly control universes with
explicit annotations in both definitions and proofs. We persevered
with frequent use of \lstinline|Set Printing Universes| and
\lstinline|Show Universes|, but we cannot recommend manual treatment
of universe levels.

Third, Coq insists that every field in a module be fully polymorphic, while in several
places we wanted to express additional constraints on the universe levels. We found a way
around it which is far from ideal. It would be helpful if we could control universe
indices and constraints in modules more precisely, although we fear having to do even more
manual bookkeeping of universes. It would be interesting to attempt to formalize
modalities in Agda or Lean, although it is not clear how this could be accomplished, since
neither seem to support the kind of polymorphism that we needed.

Despite all these problems, the modalities code in the library is
quite usable; from the outside one rarely needs to worry about the
universe issues.  In particular, the $n$-truncation is defined as a
modality, and most of the basic theorems about truncation are obtained
by specialization from general theorems about modalities.  This seems
to work quite smoothly in the rest of the library, although the user
looking for a theorem about truncations has to know to check the
general theorems about modalities.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:
