\section{Introduction}

Homotopy type theory is a novel approach to developing mathematics in Martin-L\"of’s type theory, based on interpretations of the theory into abstract
homotopy-theoretic settings such as certain higher toposes \citep{kapulkin2012simplicial,shulman2012inverse}.
%
The connection between type theory and homotopy theory is originally due to \citep{AW} and \citep{VV}.

Identity types are interpreted as path spaces, and type equivalence as homotopy
equivalence. Type-theoretic constructions correspond to homotopy-invariant
constructions on homotopy types. In addition, homotopical intuition gives rise to entirely new
type-theoretic notions, such as higher inductive types and Voevodsky's univalence axiom.
One can even develop homotopy theory in the language of type theory in a ``synthetic''
manner, treating (homotopy) types as a primitive notion.

The first formalization of homotopy type theory in a proof assistant was Voevodsky's \emph{Foundations} library
implemented in Coq, now called the \emph{UniMath} project~\citep{UniMath}. Here we present the second major such library, \emph{the HoTT library},
also implemented in Coq, with somewhat different goals from
those of UniMath. The library is freely available.\footnote{\url{http://github.com/HoTT/HoTT} or
the Coq OPAM package manager} % note: [http] instead of [https] just to avoid bad line break; the url with [http] resolves to [https] anyway

% find . -iname "*.v" | xargs coqwc
Coq word count reports that the library contains 16800 lines of specifications, 13000
lines of proofs, and 4500 lines of comments. The library is self-sufficient, completely
replacing the Coq standard library (which is incompatible with homotopy type theory) with a
bare minimum necessary for basic Coq tactics to function properly (see~\S\ref{sec:coq-changes}).

\paragraph{Contributions}
%
The HoTT library provides a substantive formalization of homotopy type theory. It
demonstrates that univalent foundations (cf.~\S\ref{Overview}) provide a workable setup
for formalization of mathematics. The library relies on advanced features of Coq
(cf.~\S\ref{sec:coq-features}), such as automatic handling of universe polymorphism
(cf.~\S\ref{univpoly}) and type classes (cf.~\S\ref{sec:type-classes}), management of
opaque and transparent definitions (cf.~\S\ref{sec:opaque}), and automation
(cf.~\S\ref{sec:automation}). We used private inductive types to implement higher
inductive types (cf.~\S\ref{HIT}), and the Coq module system to formalize modalities
(cf.~\S\ref{sec:modules}). Our development pushed Coq's abilities, which prompted the
developers to extend and modify it for our needs (cf.~\S\ref{sec:coq-changes}), and to
remove several bugs, for which we are most thankful. Overall, the success of the project
relies on careful policies and software-engineering approaches that keep the library
maintainable and usable (cf.~\S\ref{sec:software-engineering}). We relate our work to
other extensive implementations of homotopy type theory in~\S\ref{sec:related-work}.

\paragraph{Consistency}

A major concern for any piece of formalized mathematics is trust. The
HoTT library uses much more than just Martin-L\"of type theory,
including the univalence axiom, pattern matching, universe
polymorphism, type classes, private inductive types, second-class
modules, and so on; how do we know these form a consistent system?
This is a major concern not just for us, but for every user of complex
proof assistants, and has been addressed many times in the past.

In Coq, the typechecker’s kernel is the final gatekeeper. This is fairly large, in part due to
the inclusion of the module system. However, other advanced features
such as type classes and tactics are outside the kernel
and hence do not endanger consistency. So, provided we trust the
kernel, the remaining questions are the consistency of our
axioms, universe-polymorphic modules, and the implementation of private
inductive types.

There are several possible ways to tackle these questions.  So far, the primary
method available for homotopy type theory is semantic: constructing a
model of the theory in some other trusted theory (such as ZFC).  While
this has been done for various fragments of the theory,
combining them all to give a unified semantic account of homotopy type
theory together with all the features in Coq's kernel seems a daunting task.

For this reason, UniMath avoids almost all of Coq’s features (even e.g.~record types),
restricting itself as far as possible to standard Martin-L\"of
type theory (except for assuming
$\type\,{:}\,\type$ throughout, to simulate Voevodsky's resizing rules). However, this restriction cannot be
enforced by the kernel.
%
We feel rather that proof assistants and computerized formalization of mathematics are at such an
early stage that it is well worth experimenting, even at the risk of introducing an inconsistency
(which is fairly slight, due to the known semantic accounts of fragments of the theory).
In any case, the skeptical reader
should keep in mind that the standard of rigor in formalized proofs is at least a great deal
higher than the generally accepted level of rigor in traditional written mathematics.


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "hott-arxiv"
%%% End:
